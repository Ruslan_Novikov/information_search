package ru.kpfu.novikov.information_search.page_rank;

import ru.kpfu.novikov.information_search.utils.MyFileUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdjacencyMatrix {

    public static void main(String[] args) {
        int[][] matrixOfAdjacency = createdMatrixOfAdjacency();
        MyFileUtils.writeMatrixToFile(matrixOfAdjacency);
    }

    private static int[][] createdMatrixOfAdjacency() {
        int[][] matrix = new int[100][100];

        List<String> listOfUrls = MyFileUtils.readPageUrlsInWrittingOrdered();

        Map<String,Integer> pageUrlAndNum = new HashMap<>();
        int numberOfPage = 0;
        for (String url :listOfUrls) {
            pageUrlAndNum.put(url,numberOfPage);
            numberOfPage++;
        }

        for(Map.Entry<String,Integer> entry : pageUrlAndNum.entrySet()) {

            List<String> urlsInPage = MyFileUtils.readUrlsFromPage(entry.getValue());

            for(String url : urlsInPage) {
                if(pageUrlAndNum.containsKey(url)) {
                    if(!entry.getValue().equals(pageUrlAndNum.get(url))) {
                        matrix[entry.getValue()][pageUrlAndNum.get(url)] = 1;
                    }
                }
            }
        }

        return matrix;
    }

}
