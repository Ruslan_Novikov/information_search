package ru.kpfu.novikov.information_search.page_rank;

import ru.kpfu.novikov.information_search.utils.MyFileUtils;

public class PageRank {

    public int path[][] = new int[100][100];
    public double pagerank[] = new double[100];


    public void calc(double totalNodes) {

        double initialPageRank;
        double OutgoingLinks = 0;
        double dampingFactor = 0.85;
        double tempPageRank[] = new double[100];

        int externalNodeNumber;
        int internalNodeNumber;
        int k = 1;
        int iterationStep = 1;
        initialPageRank = 1 / totalNodes;
        for (k = 0; k < totalNodes; k++) {
            this.pagerank[k] = initialPageRank;
        }

        while (iterationStep < 10000) {

            for (k = 0; k < totalNodes; k++) {
                tempPageRank[k] = this.pagerank[k];
                this.pagerank[k] = 0;
            }
            for (internalNodeNumber = 0; internalNodeNumber < totalNodes; internalNodeNumber++) {
                for (externalNodeNumber = 0; externalNodeNumber < totalNodes; externalNodeNumber++) {
                    if (this.path[externalNodeNumber][internalNodeNumber] == 1) {
                        k = 1;
                        OutgoingLinks = 0;
                        while (k < totalNodes) {
                            if (this.path[externalNodeNumber][k] == 1) {
                                OutgoingLinks = OutgoingLinks + 1;
                            }
                            k = k + 1;
                        }

                        this.pagerank[internalNodeNumber] += tempPageRank[externalNodeNumber] * (1 / OutgoingLinks);
                    }
                }
            }
            iterationStep++;
        }

        // Add the Damping Factor to PageRank
        for (k = 0; k < totalNodes; k++) {
            this.pagerank[k] = (1 - dampingFactor) + dampingFactor * this.pagerank[k];
        }

        MyFileUtils.printPageRank(this.pagerank);

    }

    public static void main(String args[]) {
        PageRank p = new PageRank();
        p.path = MyFileUtils.readMatrixFromFile();
        p.calc(100);

    }

}
