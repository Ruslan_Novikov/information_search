package ru.kpfu.novikov.information_search.spider;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import ru.kpfu.novikov.information_search.utils.MyFileUtils;

import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class Spider {

    private static final int MAX_PAGES_TO_VISIT = 100;

    private List<String> pagesVisited;
    private List<String> pagesToVisit;
    private String startUrl;
    private RussianLemmatizer russianLemmatizer;
    private InvertedIndex invertedIndex;
    private Integer visitedPageNumber;
    private Map<Integer, String> pageNumberAndUrl;
    private Map<Integer, List<String>> pageToOtherPages;
    private static final Logger LOGGER = Logger.getLogger(Spider.class.getName());

    public Spider(String startUrl) {
        this.pagesVisited = new ArrayList<>();
        this.pagesToVisit = new LinkedList<>();
        this.pagesToVisit.add(startUrl);
        this.startUrl = startUrl;
        this.russianLemmatizer = new RussianLemmatizer();
        this.invertedIndex = new InvertedIndex();
        this.visitedPageNumber = 0;
        this.pageNumberAndUrl = new HashMap<>();
        this.pageToOtherPages = new HashMap<>();
    }

    private String nextUrl() {
        String nextUrl;
        do {
            nextUrl = pagesToVisit.remove(0);
        } while (pagesVisited.contains(nextUrl));
        return nextUrl;
    }

    private void search(String url) {
        try {
            Document page = Jsoup.connect(url).get();
            String pageText = page.body().text();
            MyFileUtils.writePageUrlToFile(url, visitedPageNumber, pageText);
            MyFileUtils.writeParsedTextToFile(visitedPageNumber, russianLemmatizer.lemmatizeText(pageText));
            invertedIndex.completeInvertedIndexMapFrom(pageText, visitedPageNumber);

            pageNumberAndUrl.put(visitedPageNumber, url);

            List<String> urls = page.body().select("a[href]")
                    .stream()
                    .map(link -> link.attr("href"))
                    .filter(link -> link.startsWith(startUrl))
                    .filter(link -> !link.matches(".*(\\.(css|js|gif|jpg|png|mp3|mp4|zip|gz))$"))
                    .collect(Collectors.toList());

            if (!urls.isEmpty()) {
                pagesToVisit.addAll(urls);
                pageToOtherPages.put(visitedPageNumber, urls);
                urls.size();
            }
            pagesVisited.add(url);
            visitedPageNumber++;

        } catch (IOException e) {
            e.printStackTrace();
        }
        invertedIndex.writeInvertedIndexToFile();

    }

    public void startSearch() {
        while (pagesVisited.size() < MAX_PAGES_TO_VISIT) {
            String url = nextUrl();
            search(url);
        }
        for (Map.Entry<Integer, List<String>> entry : pageToOtherPages.entrySet()) {
            Integer key = entry.getKey();
            List<String> value = entry.getValue();
            MyFileUtils.writeLinkToAnotherPage(key, value);
        }

        MyFileUtils.writePagesVisitedUrls(pagesVisited);

    }

}
