package ru.kpfu.novikov.information_search.spider;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

class InvertedIndex {

    private  Map<String, StringBuilder> invertedIndexMap = new HashMap<>();
    private static final String INVERTED_INDEX_PATH = "src/main/resources/inverted_index/inverted_index.txt";

    void completeInvertedIndexMapFrom(String text, int pageNumber) {
        String[] firstWords = new RussianLemmatizer().lemmatizeText(text).split("\n");
        Set<String> setOfWords = new HashSet<>();
        for(String word : firstWords) {
            setOfWords.add(word);
        }
        for (String word : setOfWords) {
            StringBuilder value = invertedIndexMap.get(word);
            if (value != null) {
                invertedIndexMap.put(word, value.append(", ").append(pageNumber));
            } else {
                invertedIndexMap.put(word, new StringBuilder().append(pageNumber));
            }
        }

    }

    void writeInvertedIndexToFile() {
        try (FileWriter writer = new FileWriter(new File(INVERTED_INDEX_PATH))) {
            for (Map.Entry entry : invertedIndexMap.entrySet()) {
                writer.write(entry.getKey() + " : " + entry.getValue() + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
