package ru.kpfu.novikov.information_search.spider;


import ru.kpfu.novikov.information_search.utils.MyFileUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class TfIdf {

    private static final String PARSED_PAGES_PATH = "src/main/resources/parsed_pages/";
    private static final String TF_PATH = "src/main/resources/tf/";
    private static final String UNIQUE_WORDS_IDFS_FILES_PATH = "src/main/resources/unique_words_idf/";


    int numberOfPages;

    //Map слову сооветствует его idf
    private Map<String, Float> idfMap = new HashMap<>();

    public TfIdf(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    // количество повторений слова в документе, деленное на количество всех слов в документе
    public double tf(List<String> doc, String term) {
        double result = 0;
        for (String word : doc) {
            if (term.equalsIgnoreCase(word))
                result++;
        }
        return result / numberOfPages;
    }

    //Количество документов, деленное на количество документов со словом
    public double idf(List<List<String>> docs, String term) {
        if (idfMap.get(term) != null) {
            return idfMap.get(term);
        }
        float n = 0;
        for (List<String> doc : docs) {
            for (String word : doc) {
                if (term.equalsIgnoreCase(word)) {
                    n++;
                    break;
                }
            }
        }
        float idf = numberOfPages / n;
        idfMap.put(term, idf);
        return idf;
    }

    public void writeIdfsOfUniqueWords() {
        File directory = new File(PARSED_PAGES_PATH);
        File[] fileArray = directory.listFiles();
        List<List<String>> listOfTexts = new ArrayList();
        Set<String> uniqueWordSet = new HashSet<>();
        for (File file : fileArray) {
            listOfTexts.add(getFileContent(file));
            uniqueWordSet.addAll(getFileContent(file));
        }
        for (String uniqueWord : uniqueWordSet) {
            try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(UNIQUE_WORDS_IDFS_FILES_PATH + uniqueWord + ".txt"), true))) {
                bufferedWriter.write(idf(listOfTexts, uniqueWord) + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


    public void writeTfsOfUniqueWords(Set<String> uniqueWordSet, int numberOfPage) {
        File file = new File(PARSED_PAGES_PATH + numberOfPage + ".txt");
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(TF_PATH + numberOfPage + ".txt"), true))) {
            for (String uniqueWord : uniqueWordSet) {
                bufferedWriter.write(tf(getFileContent(file), uniqueWord) + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public List<String> getFileContent(File file) {
        List<String> content = new ArrayList<>();
        String line;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            while ((line = reader.readLine()) != null) {
                content.add(line.trim());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }

    public Map<Integer, Float> tfUniqueMap(int pageNumber) {
        Map<Integer,Float> tfMap = new HashMap<>();
        int index = 0;
        try {
            List<String> allLines = Files.readAllLines(Paths.get(TF_PATH + pageNumber + ".txt"));
            for (String line : allLines) {
                tfMap.put(index++,Float.parseFloat(line));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tfMap;
    }

    public List<List<Double>> countTfIdfForUniqueWords(Set<String> uniqueWords) {
        List<String> words = new ArrayList<>(uniqueWords);
        LinkedList<List<Double>> tfIdfArray = new LinkedList<>();
        for (int i = 0; i < 100; i++) {
            tfIdfArray.add(new ArrayList<>());
            List lastArray = tfIdfArray.getLast();

            Map<Integer, Float> tfMap = tfUniqueMap(i);
            for (int j = 0; j < uniqueWords.size(); j++) {
                lastArray.add(tfMap.get(j) * MyFileUtils.getIdf(words.get(j)));
            }
        }

        return tfIdfArray;
    }

    public List<Double> countTfIdfForQueriedVector(Set<String> uniqueWords, String queriedVector) {
        List<String> words = new ArrayList<>(uniqueWords);
        List<Double> tfIdfArray = new ArrayList<>();
        List<List<String>> parsedPagesList = MyFileUtils.readParsedPages();
        for (int j = 0; j < words.size(); j++) {
            tfIdfArray.add(tf(Arrays.asList(queriedVector.split(" ")), words.get(j)) * idf(parsedPagesList, words.get(j)));
        }
        return tfIdfArray;
    }
}
