package ru.kpfu.novikov.information_search.spider;


import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.ru.RussianAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.IOException;

public class RussianLemmatizer {

    public String lemmatizeText(String documentText) {
        StringBuilder builder = new StringBuilder();
        try {
            RussianAnalyzer russianAnalyzer = new RussianAnalyzer();
            TokenStream tokenStream = russianAnalyzer.tokenStream("contents", documentText);
            CharTermAttribute attribute = tokenStream.addAttribute(CharTermAttribute.class);
            tokenStream.reset();
            while (tokenStream.incrementToken()) {
                String term = attribute.toString();
                builder.append(term + "\n");
            }
            tokenStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return builder.toString();
    }
}
