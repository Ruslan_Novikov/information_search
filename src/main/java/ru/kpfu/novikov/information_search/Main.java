package ru.kpfu.novikov.information_search;

import ru.kpfu.novikov.information_search.spider.Spider;
import ru.kpfu.novikov.information_search.spider.TfIdf;
import ru.kpfu.novikov.information_search.utils.MyFileUtils;

import java.util.List;
import java.util.Set;

public class Main {

    private static final String startUrl = "https://habr.com/ru/";

    public static void main(String[] args) {
        Spider spider = new Spider(startUrl);
        spider.startSearch();

       TfIdf counter = new TfIdf(100);
////        counter.writeTfIdfForAllDocs();
        counter.writeIdfsOfUniqueWords();
//
        Set<String> uniqueWords = MyFileUtils.readUniqueWordsFromFiles();

//        List<List<String>> lists = MyFileUtils.readParsedPages();

        for(int i = 0 ; i < 100 ; i ++) {
            counter.writeTfsOfUniqueWords(uniqueWords,i);
        }
    }
}
