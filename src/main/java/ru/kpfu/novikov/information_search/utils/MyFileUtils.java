package ru.kpfu.novikov.information_search.utils;

import org.apache.commons.io.FileUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MyFileUtils {

    private static final String URLS_OF_PAGES_PATH = "src/main/resources/urls/urls.txt";
    private static final String PAGES_TEXTS = "src/main/resources/page_texts/";
    private static final String PARSED_PAGES_PATH = "src/main/resources/parsed_pages/";
    private static final String MATRIX_PATH = "src/main/resources/adjacency_matrix/adjacencyMatrix.txt";
    private static final String PAGE_WITH_URLS = "src/main/resources/page_with_urls/";
    private static final String PAGE_RANK_PATH = "src/main/resources/page_rank/pageRank.txt";
    private static final String COSINE_SIMILLARITY_FILE_PATH = "src/main/resources/cosine_simillarity/cosine_simillarity.txt";
    private static final String UNIQUE_WORDS_IDFS_FILES_PATH = "src/main/resources/unique_words_idf/";

    public static void writePageUrlToFile(String url, int pageNumber, String text) {
        try (FileWriter fileWriter = new FileWriter(new File(URLS_OF_PAGES_PATH), true)) {
            fileWriter.write(url + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (BufferedWriter tsw = new BufferedWriter(new FileWriter(new File(PAGES_TEXTS + pageNumber + ".txt")))) {
            tsw.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeParsedTextToFile(int pageNumber, String text) {
        try (BufferedWriter tsw = new BufferedWriter(new FileWriter(new File(PARSED_PAGES_PATH + pageNumber + ".txt")))) {
            tsw.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<List<String>> readParsedPages() {
        List<List<String>> listOfPages = new ArrayList<>();
        for (File file : Objects.requireNonNull(new File(PARSED_PAGES_PATH).listFiles())) {
            List<String> wordsOfPage = null;
            try {
                wordsOfPage = Arrays.asList(FileUtils.readFileToString(file, "UTF-8").split("\n"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            listOfPages.add(wordsOfPage);
        }
        return listOfPages;
    }


    public static void writeLinkToAnotherPage(int currentPageNumber, List<String> urls) {
        try (FileOutputStream fos = new FileOutputStream(new File(PAGE_WITH_URLS + currentPageNumber + ".txt"))) {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
            for (String url : urls) {
                bw.write(url + '\n');
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writePagesVisitedUrls(List<String> pagesVisited) {
        try (FileOutputStream fos = new FileOutputStream(new File(URLS_OF_PAGES_PATH))) {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
            for (String s : pagesVisited) {
                bw.write(s + '\n');
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<String> readPageUrlsInWrittingOrdered() {
        List<String> pageUrls = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(URLS_OF_PAGES_PATH))) {
            String line;
            while ((line = reader.readLine()) != null) {
                pageUrls.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pageUrls;
    }


    public static List<String> readUrlsFromPage(Integer pageNumber) {
        List<String> pageUrls = new ArrayList<>();
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(PAGE_WITH_URLS + pageNumber + ".txt"));
            String line;
            while ((line = reader.readLine()) != null) {
                pageUrls.add(line);
            }
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return pageUrls;
    }

    public static void writeMatrixToFile(int[][] matrix) {
        try (FileOutputStream fos = new FileOutputStream(new File(MATRIX_PATH))) {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[i].length; j++) {
                    bw.write(Integer.toString(matrix[i][j]) + " ");
                }
                bw.write('\n');
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static int[][] readMatrixFromFile() {
        int rows = 100;
        int columns = 100;
        int[][] myArray = new int[rows][columns];
        Scanner sc = null;
        try {
            sc = new Scanner(new BufferedReader(new FileReader(MATRIX_PATH)));
            while (sc.hasNextLine()) {
                for (int i = 0; i < myArray.length; i++) {
                    String[] line = sc.nextLine().trim().split(" ");
                    for (int j = 0; j < line.length; j++) {
                        myArray[i][j] = Integer.parseInt(line[j]);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return myArray;
    }

    public static void printPageRank(double[] pagerank) {
        try (FileOutputStream fos = new FileOutputStream(new File(PAGE_RANK_PATH))) {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
            for (int i = 0; i < pagerank.length; i++) {
                bw.write(String.valueOf(pagerank[i]));
                bw.newLine();
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Set<String> readUniqueWordsFromFiles() {
        Set<String> uniqueWords = new HashSet<>();
        try (Stream<Path> walk = Files.walk(Paths.get(PARSED_PAGES_PATH))) {
            List<String> resultFilePaths = walk.filter(Files::isRegularFile).map(Path::toString).collect(Collectors.toList());
            for (String s : resultFilePaths) {
                File file = new File(s);
                try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        uniqueWords.add(line);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return uniqueWords;
    }

    public static void writeCosineSimillarity(Map<Integer, Float> mapOfCosinesSimillarity) {
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(COSINE_SIMILLARITY_FILE_PATH))))) {
            for (Map.Entry<Integer, Float> entry : mapOfCosinesSimillarity.entrySet()) {
                bw.write(entry.getKey() + " : " + entry.getValue() + '\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static double getIdf(String s) {
        double num = 0d;
        try (Scanner sc = new Scanner(new BufferedReader(new FileReader(new File(UNIQUE_WORDS_IDFS_FILES_PATH + s + ".txt"))))) {

            while (sc.hasNext()) {
                if (sc.hasNextDouble()) {
                    num = sc.nextDouble();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return num;
    }
}
