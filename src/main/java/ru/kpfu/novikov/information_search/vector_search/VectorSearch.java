package ru.kpfu.novikov.information_search.vector_search;

import ru.kpfu.novikov.information_search.spider.TfIdf;
import ru.kpfu.novikov.information_search.utils.MyFileUtils;

import static java.util.stream.Collectors.*;

import java.util.*;

public class VectorSearch {

    public static void main(String[] args) {
        Set<String> uniqueWords = MyFileUtils.readUniqueWordsFromFiles();

        TfIdf counter = new TfIdf(100);
        List<List<Double>> tfIdfMatrix = counter.countTfIdfForUniqueWords(uniqueWords);

        List<Double> tfIdfForQueriedString = counter.countTfIdfForQueriedVector(uniqueWords,"ещё аккаунт");

        Map<Integer, Float> cosinesSimilarityMap = new HashMap<>();

        for(int i = 0 ; i < tfIdfMatrix.size(); i++) {
               cosinesSimilarityMap.put(i,(float) calculateCousineSimilarity(tfIdfMatrix.get(i),tfIdfForQueriedString));
        }

        MyFileUtils.writeCosineSimillarity(mapSortedByValues(cosinesSimilarityMap));
    }

    public static double calculateCousineSimilarity(List<Double> first, List<Double> second) {
        if(first.size() != second.size()) {
            throw new RuntimeException("Lengths of vectors must be equal");
        }

        double scalarComposition = 0d;
        double sumOfSquaresFirst = 0d;
        double sumOfSquaresSecond = 0d;

        for(int i = 0 ; i < first.size(); i++) {
            double firstD =  first.get(i);
            double secondD = second.get(i);
            scalarComposition = scalarComposition + firstD * secondD;
            sumOfSquaresFirst = sumOfSquaresFirst + firstD * firstD;
            sumOfSquaresSecond = sumOfSquaresSecond + secondD * second.get(i);
        }
        return scalarComposition / (Math.sqrt(sumOfSquaresFirst) * Math.sqrt(sumOfSquaresSecond));
    }

    private static LinkedHashMap<Integer,Float> mapSortedByValues(Map<Integer, Float> initialMap) {
        return initialMap
                .entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(
                        toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
                                LinkedHashMap::new));
    }

}
